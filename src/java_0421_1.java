import java.util.Scanner;

public class java_0421_1 {
    static void order(String food){
        System.out.println("You have ordered "+food+". Thank you!");
    }
    public static void main(String[] args) {

        /*String[] food = new String[4];
        food[1] = "Tempura";
        food[2] = "Ramen";
        food[3] = "Udon";*/

        Scanner userIn = new Scanner(System.in);
        System.out.println("What would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.println("Your order [1-3]:");

        int number = userIn.nextInt();
        userIn.close();

        /*System.out.println("We have received your order for "+food[number]+"! It will be served soon!");*/

        if (number == 1) {
            order("Tempura");
        }
        else if (number == 2) {
            order("Ramen");
        }
        else if (number == 3) {
            order("Udon");
        }
    }
}
